﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAroundPiratShip : MonoBehaviour
{
    public int maxEnemyCount = 10;
    public int minEnemyCount = 2;
    public int increaseEnemyCountAfterSeconds=10;
    public int sphereRadius = 1;

    public List<Transform> spawnPoints = new List<Transform>();
    public GameObject enemy;

    private int currentEnemyCount;
    private int currentlyAllowedEnemys;
    // Start is called before the first frame update
    void Start()
    {
        currentEnemyCount = 0;
        currentlyAllowedEnemys = minEnemyCount;
        StartCoroutine(IncreaseCurrentlyAllowedEnemysOverTime());
    }

    // Update is called once per frame
    void Update()
    {
        spawnNewEnemy();
    }

    void spawnNewEnemy()
    {
        if(currentEnemyCount < currentlyAllowedEnemys)
        {

                Debug.Log("Spawn Enemy");
                int randomIndex = Random.Range(0, spawnPoints.Count - 1);
                var newEnemy = Instantiate(enemy, spawnPoints[randomIndex].transform.position, Quaternion.identity);
        newEnemy.GetComponent<MoveEnemy>().target = transform;
                currentEnemyCount++;
            

        }
    }


    IEnumerator IncreaseCurrentlyAllowedEnemysOverTime()
    {
        while (currentlyAllowedEnemys <= maxEnemyCount)
        {
            yield return new WaitForSeconds(increaseEnemyCountAfterSeconds);
            currentlyAllowedEnemys++;
        }
        yield break;
    }



    
}

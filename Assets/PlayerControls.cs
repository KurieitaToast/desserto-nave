// GENERATED AUTOMATICALLY FROM 'Assets/PlayerControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerControls"",
    ""maps"": [
        {
            ""name"": ""Player0"",
            ""id"": ""a6091dbb-70cd-4d83-a1aa-513e1da1a736"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""PassThrough"",
                    ""id"": ""0f791e7f-08b2-4689-85cd-0a218e37a0df"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Enter"",
                    ""type"": ""Button"",
                    ""id"": ""4cf681fd-6ab0-492d-ae4d-515802f0f25b"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Cancel"",
                    ""type"": ""Button"",
                    ""id"": ""00e80196-c1c4-46ce-aebe-0590394baeb3"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Any"",
                    ""type"": ""Button"",
                    ""id"": ""8267c33d-7383-45a7-93e0-c325283d87e2"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Restart"",
                    ""type"": ""Button"",
                    ""id"": ""231c8239-bc59-4060-b1a8-434e96a5f44b"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""b914a96e-2bf9-4528-90f1-9916c2a187e4"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""WASD"",
                    ""id"": ""45df1997-912f-40bc-b12a-bed3fdad847f"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""9f66d2b0-10ac-4591-b883-1987723f1478"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""bed98e46-4f43-4c7b-a7e9-fc5af0b2552c"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""73ee2178-585c-4cf0-8967-ff3328dd58a8"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""f1de014b-2cc3-4ec6-a2a6-6ac8022a4e51"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""dfdc9a95-4a6c-4074-b53c-335a67c62d45"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""19074b01-a5b4-4d9a-b810-03b6c6ddb839"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Enter"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a830f51c-813f-492a-80c6-5c36d6298ede"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Enter"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e27e9b71-e2a1-4a71-a167-d4afcfe674a0"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Enter"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""794c4a18-1bf2-4087-9fa1-60ae98d11567"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Cancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d42cdf04-08f2-43b1-a78f-cde99f396c04"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Cancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""54798450-7f79-4dfd-9681-45ab5ec59eef"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Cancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5138bc48-3d15-4856-9b13-59af7ff6a3a4"",
                    ""path"": ""<Keyboard>/anyKey"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Any"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1eb27f97-d14a-4bf4-b6cd-b74ed6897945"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Any"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""97b63c70-f079-421b-b705-cdaacef9c47f"",
                    ""path"": ""<Gamepad>/dpad"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Any"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4e04836b-2278-43e2-a328-be20dc917f18"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Restart"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""347a0397-e5ed-4269-840a-bcdcec59c177"",
                    ""path"": ""<Gamepad>/select"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Restart"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""20b806e3-a63d-481c-84bc-43cc67e370a5"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Restart"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Player0
        m_Player0 = asset.FindActionMap("Player0", throwIfNotFound: true);
        m_Player0_Move = m_Player0.FindAction("Move", throwIfNotFound: true);
        m_Player0_Enter = m_Player0.FindAction("Enter", throwIfNotFound: true);
        m_Player0_Cancel = m_Player0.FindAction("Cancel", throwIfNotFound: true);
        m_Player0_Any = m_Player0.FindAction("Any", throwIfNotFound: true);
        m_Player0_Restart = m_Player0.FindAction("Restart", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Player0
    private readonly InputActionMap m_Player0;
    private IPlayer0Actions m_Player0ActionsCallbackInterface;
    private readonly InputAction m_Player0_Move;
    private readonly InputAction m_Player0_Enter;
    private readonly InputAction m_Player0_Cancel;
    private readonly InputAction m_Player0_Any;
    private readonly InputAction m_Player0_Restart;
    public struct Player0Actions
    {
        private @PlayerControls m_Wrapper;
        public Player0Actions(@PlayerControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_Player0_Move;
        public InputAction @Enter => m_Wrapper.m_Player0_Enter;
        public InputAction @Cancel => m_Wrapper.m_Player0_Cancel;
        public InputAction @Any => m_Wrapper.m_Player0_Any;
        public InputAction @Restart => m_Wrapper.m_Player0_Restart;
        public InputActionMap Get() { return m_Wrapper.m_Player0; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(Player0Actions set) { return set.Get(); }
        public void SetCallbacks(IPlayer0Actions instance)
        {
            if (m_Wrapper.m_Player0ActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_Player0ActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_Player0ActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_Player0ActionsCallbackInterface.OnMove;
                @Enter.started -= m_Wrapper.m_Player0ActionsCallbackInterface.OnEnter;
                @Enter.performed -= m_Wrapper.m_Player0ActionsCallbackInterface.OnEnter;
                @Enter.canceled -= m_Wrapper.m_Player0ActionsCallbackInterface.OnEnter;
                @Cancel.started -= m_Wrapper.m_Player0ActionsCallbackInterface.OnCancel;
                @Cancel.performed -= m_Wrapper.m_Player0ActionsCallbackInterface.OnCancel;
                @Cancel.canceled -= m_Wrapper.m_Player0ActionsCallbackInterface.OnCancel;
                @Any.started -= m_Wrapper.m_Player0ActionsCallbackInterface.OnAny;
                @Any.performed -= m_Wrapper.m_Player0ActionsCallbackInterface.OnAny;
                @Any.canceled -= m_Wrapper.m_Player0ActionsCallbackInterface.OnAny;
                @Restart.started -= m_Wrapper.m_Player0ActionsCallbackInterface.OnRestart;
                @Restart.performed -= m_Wrapper.m_Player0ActionsCallbackInterface.OnRestart;
                @Restart.canceled -= m_Wrapper.m_Player0ActionsCallbackInterface.OnRestart;
            }
            m_Wrapper.m_Player0ActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Enter.started += instance.OnEnter;
                @Enter.performed += instance.OnEnter;
                @Enter.canceled += instance.OnEnter;
                @Cancel.started += instance.OnCancel;
                @Cancel.performed += instance.OnCancel;
                @Cancel.canceled += instance.OnCancel;
                @Any.started += instance.OnAny;
                @Any.performed += instance.OnAny;
                @Any.canceled += instance.OnAny;
                @Restart.started += instance.OnRestart;
                @Restart.performed += instance.OnRestart;
                @Restart.canceled += instance.OnRestart;
            }
        }
    }
    public Player0Actions @Player0 => new Player0Actions(this);
    public interface IPlayer0Actions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnEnter(InputAction.CallbackContext context);
        void OnCancel(InputAction.CallbackContext context);
        void OnAny(InputAction.CallbackContext context);
        void OnRestart(InputAction.CallbackContext context);
    }
}

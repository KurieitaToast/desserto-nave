﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputTest : MonoBehaviour
{
    [SerializeField]private float speed;
    private Vector2 dir;

    private void OnMove(InputValue value)
    {
        Debug.Log(value.Get<Vector2>());
    }

    private void OnEnter(InputValue value)
    {
        Debug.Log("E");
    }

    private void OnCancel(InputValue value)
    {
        Debug.Log("C");
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Random = UnityEngine.Random;

public class SpawnPlayer : MonoBehaviour
{
    public Collider spawnArea;
    private PlayerAudioManager audioManager;
    private void Start()
    {
        spawnArea = GetComponent<Collider>();
        audioManager = GetComponent<PlayerAudioManager>();
    }

    private void OnPlayerJoined(PlayerInput input)
    {
        input.transform.position = RandomPointInBounds(spawnArea.bounds);
        input.gameObject.GetComponentInChildren<SkinnedMeshRenderer>().materials[2].color = Random.ColorHSV(0, 1);
        input.gameObject.GetComponent<PlayerAudio>().VoiceLines = audioManager.PopRandomLines();
        GetComponent<PlayerAudioManager>().playerAudios.Add(input.gameObject.GetComponent<PlayerAudio>());
    }
    
    public static Vector3 RandomPointInBounds(Bounds bounds) {
        return new Vector3(
            Random.Range(bounds.min.x, bounds.max.x),
            Random.Range(bounds.min.y, bounds.max.y),
            Random.Range(bounds.min.z, bounds.max.z)
        );
    }

}

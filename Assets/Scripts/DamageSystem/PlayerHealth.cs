﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.DamageSystem
{
  public class PlayerHealth : Health
  {
    public override void Die()
    {
      Destroy(gameObject);
    }
  }
}
﻿using Assets.Scripts.DamageSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Damageable : MonoBehaviour, IDamageable
{
  [SerializeField]
  private int health;
  public int Health
  {
    get { return health; }
    set { health = Mathf.Max(0,value); }
  }

    public GameObject damageAnimation;
    public GameObject deathAnimation;

  public virtual void TakeDamage(int damage, DamageType damageType)
  {
    if (damageType == DamageType.Absolute)
    {
      Health -= damage;
      Debug.Log($"Ouch! Took {damage} damage! {Health} health left.");
    }
    else
    {
      float newHealth = Health * (damage / 100f);
      Health = (int)newHealth;
      Debug.Log($"Ouch! Took {damage}% damage! {Health} health left.");
    }
    
    if (Health <= 0)
    {
      Debug.Log($"Arrr! This is the end!");
      Die();
    }

        //var particelSystems = damageAnimation.GetComponentsInChildren<ParticleSystem>();
        //if(particelSystems != null)
        //{
        //    foreach(ParticleSystem parSys in particelSystems)
        //    {
        //        Debug.Log("parsys should play");
        //        parSys.Play();
        //    }
        //}

        damageAnimation.GetComponent<ParticlePlayer>().Play();
  }

  public abstract void Die();

  void Start()
  {
  }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.DamageSystem
{
  public abstract class Health : Damageable, IRepairable
  {
    [SerializeField]
    private int maxHealth;
    public int MaxHealth
    {
      get { return maxHealth; }
      set { maxHealth = value; }
    }

    public virtual void Repair(int damageToRepair)
    {
      var newHealth = Health + damageToRepair;
            if (newHealth > MaxHealth)
                Health = MaxHealth;
            else
                Health = newHealth;

            if (Health == MaxHealth)
            {
                deathAnimation.GetComponent<ParticlePlayer>().Stop();
                damageAnimation.GetComponent<ParticlePlayer>().Stop();
            }

            Debug.Log($"Arrr got repaired. New Health is {Health}");
    }
  }
}
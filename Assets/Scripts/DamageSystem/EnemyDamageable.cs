﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.DamageSystem
{
    public class EnemyDamageable : Damageable
    {
        public override void Die()
        {
            GameObject ga = Instantiate(deathAnimation, transform.position, Quaternion.identity);
            ga.GetComponent<ParticlePlayer>().Play();
            //var particelSystems = ga.GetComponentsInChildren<ParticleSystem>();
            //if (particelSystems != null)
            //{
            //    foreach (ParticleSystem parSys in particelSystems)
            //    {
            //        parSys.Play();
            //    }
            //}
            Destroy(gameObject);
        }

        
    }

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.DamageSystem
{
  public interface IRepairable
  {
    int MaxHealth { get; set; }
    void Repair(int damageToRepair);
  }
}
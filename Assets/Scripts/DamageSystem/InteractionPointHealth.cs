﻿using UnityEngine;

namespace Assets.Scripts.DamageSystem
{
    public class InteractionPointHealth : Health
    {
        public override void TakeDamage(int damage, DamageType damageType)
        {
            base.TakeDamage(damage, damageType);

            MeshRenderer[] rends = GetComponentsInChildren<MeshRenderer>();


            foreach (MeshRenderer rend in rends)
            {
                rend.material.color = Color.Lerp(Color.red, Color.white, Health / (float) MaxHealth);
            }
        }

        public override void Repair(int damageToRepair)
        {
            base.Repair(damageToRepair);
            
            MeshRenderer[] rends = GetComponentsInChildren<MeshRenderer>();


            foreach (MeshRenderer rend in rends)
            {
                rend.material.color = Color.Lerp(Color.red, Color.white, Health / (float) MaxHealth);
            }

        }

        public override void Die()
        {
            deathAnimation.GetComponent<ParticlePlayer>().Play();
            GetComponent<Repair>().Active = true;
            VoiceLineType type = VoiceLineType.ENGINE_BROKEN;

            if (GetComponent<SteeringWheel>() != null)
            {
                type = VoiceLineType.CONTROL_BROKEN;
            }
            else if(GetComponent<InteractableCannon>() != null)
            {
                type = VoiceLineType.CANONE_BROKEN;
            }
            
            PlayerAudioManager.Instance.playerAudios[Random.Range(0,PlayerAudioManager.Instance.playerAudios.Count)].PlayVoiceLine(type);
        }
    }
}
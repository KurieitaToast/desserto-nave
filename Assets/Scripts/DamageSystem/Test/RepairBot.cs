﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.DamageSystem.Test
{
  public class RepairBot : MonoBehaviour
  {
    public GameObject target;
    public int damageToRepair = 1;
    public int repairRate = 3;

    void Start()
    {
      StartCoroutine("Repair");
    }

    IEnumerator Repair()
    {
      if (target.GetComponent<Health>() == null)
        yield break;

      while (true)
      {
        if (target == null)
          yield break;

        target.GetComponent<Health>().Repair(damageToRepair);
        yield return new WaitForSeconds(repairRate);
      }
    }
  }
}
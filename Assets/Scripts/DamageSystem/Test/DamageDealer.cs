﻿using Assets.Scripts.DamageSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.DamageSystem.Test
{
  public class DamageDealer : MonoBehaviour
  {
    public GameObject damageable;
    public GameObject repairable;
    public int damage;
    public DamageType damageType;

    void Update()
    {
      if (Input.GetKeyDown(KeyCode.Space))
      {
        if (damageable != null)
          damageable.GetComponent<Damageable>().TakeDamage(damage, damageType);
        
        if (repairable != null)
          repairable.GetComponent<Health>().TakeDamage(damage, damageType);
      }
    }
  }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
  public float speed;

  public bool RotateXAxis;
  public bool RotateYAxis;
  public bool RotateZAxis;

  void Update()
  {
    transform.Rotate
    (
      RotateXAxis ? speed * Time.deltaTime : 0,
      RotateYAxis ? speed * Time.deltaTime : 0,
      RotateZAxis ? speed * Time.deltaTime : 0,
      Space.Self
    );
  }
}

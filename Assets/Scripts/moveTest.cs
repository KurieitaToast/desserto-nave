﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveTest : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 position = transform.position;

        if (Input.GetKeyDown(KeyCode.W))
        {
            position.x--;
            transform.position = position;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            position.x++;
            transform.position = position;
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            position.z--;
            transform.position = position;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            position.z++;
            transform.position = position;
        }

    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ShipMovement : MonoBehaviour
{
    
    public float shipMoveSpeed = 2f;
    public float rotationSpeed = 0.01f;
    public float accSpeed = 0.1f;
    public float maxAcc = 2f;
    private Camera _cam;
    private Rigidbody _rb;
    private float angle = 0;
    private float acc;

    //public Transform ship;

    //private Vector2 _move = Vector2.zero;
    //private Vector3 _moveDirection = new Vector3();
    //private Vector3 _move3D = new Vector3();
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        _rb.MovePosition(transform.position + (transform.forward * Time.fixedDeltaTime * shipMoveSpeed));

        //acc += Mathf.Sin(Time.time) * accSpeed * Time.deltaTime;
       // Mathf.Clamp(acc, -maxAcc, maxAcc);
       
       //Random rotation angle changes continuous over time
       angle = Mathf.Sin(Time.time * 0.5f) * 90f * rotationSpeed * Time.fixedDeltaTime;
        
        _rb.MoveRotation(transform.rotation*Quaternion.AngleAxis(angle,transform.up));
        
        //Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(Vector3.right), 20);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ScrapSpawn : MonoBehaviour
{
    public GameObject scrapPrefab;
    public float spawnTimer = 5f;
    public int spawnCount = 3;
    private Collider col;
    

    private void Start()
    {
        col = GetComponent<Collider>();
        StartCoroutine(AutoSpawn());
    }

    private IEnumerator AutoSpawn()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnTimer);
            Spawn(spawnCount);
        }

    }

    public void Spawn(int count)
    {
        for(int i = 0; i< count;i++) {
            Instantiate(scrapPrefab, RandomPointInBounds(col.bounds), Quaternion.identity);
        }
    }
    
    private static Vector3 RandomPointInBounds(Bounds bounds) {
        return new Vector3(
            Random.Range(bounds.min.x, bounds.max.x),
            Random.Range(bounds.min.y, bounds.max.y),
            Random.Range(bounds.min.z, bounds.max.z)
        );
    }
}

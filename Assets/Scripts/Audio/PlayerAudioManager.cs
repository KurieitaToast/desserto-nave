﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerAudioManager : MonoBehaviour
{
    public static PlayerAudioManager Instance = null;
    public List<PlayerAudio> playerAudios;
    public List<CharacterVoiceLines> VoiceLineses;
    private List<CharacterVoiceLines> avaibleLines;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        if (Instance != this)
        {
            Destroy(gameObject);
        }

        avaibleLines = new List<CharacterVoiceLines>();
        foreach (CharacterVoiceLines cvl in VoiceLineses)
        {
            avaibleLines.Add(cvl);
        }
    }

    public CharacterVoiceLines PopRandomLines()
    {
        if (avaibleLines.Count <= 0)
        {
            foreach (CharacterVoiceLines cvl2 in VoiceLineses)
            {
                avaibleLines.Add(cvl2);
            }
        }

        int idx = Random.Range(0, avaibleLines.Count);
        CharacterVoiceLines cvl = avaibleLines[idx];
        avaibleLines.RemoveAt(idx);
        return cvl;
    }
}
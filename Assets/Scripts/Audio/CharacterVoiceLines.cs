﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(menuName = "CharacterVoiceLines")]
public class CharacterVoiceLines : ScriptableObject
{
    public List<AudioClip> shootLines;
    public List<AudioClip> repairLines;
    public List<AudioClip> noMatLines;
    public List<AudioClip> takeControlLines;
    public List<AudioClip> leaveControlLines;
    public List<AudioClip> engineBrokenLines;
    public List<AudioClip> canoneBrokenLines;
    public List<AudioClip> controlBrokenLines;
    public List<AudioClip> randomLines;

    public AudioClip GetClip(VoiceLineType type)
    {
        switch (type)
        {
            case VoiceLineType.SHOOT:
                return shootLines[Random.Range(0, shootLines.Count)];
            case VoiceLineType.REPAIR:
                return repairLines[Random.Range(0, repairLines.Count)];
            case VoiceLineType.NO_MAT:
                return noMatLines[Random.Range(0, noMatLines.Count)];
            case VoiceLineType.TAKE_CONTROL:
                return takeControlLines[Random.Range(0, takeControlLines.Count)];
            case VoiceLineType.LEAVE_CONTROL:
                return leaveControlLines[Random.Range(0, leaveControlLines.Count)];
            case VoiceLineType.ENGINE_BROKEN:
                return engineBrokenLines[Random.Range(0, engineBrokenLines.Count)];
            case VoiceLineType.CANONE_BROKEN:
                return canoneBrokenLines[Random.Range(0, canoneBrokenLines.Count)];
            case VoiceLineType.CONTROL_BROKEN:
                return controlBrokenLines[Random.Range(0, controlBrokenLines.Count)];
            case VoiceLineType.RANDOM:
                return randomLines[Random.Range(0, randomLines.Count)];
            default:
                throw new ArgumentOutOfRangeException(nameof(type), type, null);
        }
    }
    
}

public enum VoiceLineType
{
    SHOOT,
    REPAIR,
    NO_MAT,
    TAKE_CONTROL,
    LEAVE_CONTROL,
    ENGINE_BROKEN,
    CANONE_BROKEN,
    CONTROL_BROKEN,
    RANDOM
}

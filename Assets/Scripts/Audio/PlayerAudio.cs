﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudio : MonoBehaviour
{
    public CharacterVoiceLines VoiceLines;
    private AudioSource source;

    private void Awake()
    {
        source = GetComponent<AudioSource>();

    }

    public void PlayVoiceLine(VoiceLineType type)
    {
        AudioClip clip = VoiceLines.GetClip(type);
        source.clip = clip;
        source.Play();
    }
}

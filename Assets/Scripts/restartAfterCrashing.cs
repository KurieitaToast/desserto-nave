﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartAfterCrashing : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision col)
    {
        StartCoroutine(gameover(col.gameObject));


    }

    private IEnumerator gameover(GameObject go)
    {
        Time.timeScale = 0.5f;
        yield return new WaitForSeconds(2);
        Destroy(go.transform.parent);
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(0); 
    }
}

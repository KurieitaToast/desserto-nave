﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.DamageSystem;

public class MakeBulletDamage : MonoBehaviour
{

    public int hitCount;
    public int damage;
  public GameObject bulletDestroyAnimation;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "PiratShip")
        {
            var interactionPointHealths = collision.gameObject.GetComponentsInChildren<InteractionPointHealth>();
            while (hitCount > 0)
            {
                int rnd = Random.Range(0, interactionPointHealths.Length);
                interactionPointHealths[rnd].TakeDamage(1, DamageType.Absolute);
                hitCount--;
            }
        }
        else
        {
            IDamageable[] targets = collision.gameObject.GetComponentsInChildren<IDamageable>();
            foreach (var tar in targets)
            {
                tar.TakeDamage(damage,DamageType.Absolute);
            }
        }


    GameObject ga = Instantiate(bulletDestroyAnimation, transform.position, Quaternion.identity);
    var pp = ga.GetComponent<ParticlePlayer>();
    if (pp != null)
    {
      pp.scale = new Vector3(0.2f, 0.2f, 0.2f);
      pp.Play();
    }

    Destroy(gameObject);
    }

    void OnDestroy()
    {
        Debug.Log("OnDestryBullet");
        //playExplosionAnimation
    }
}

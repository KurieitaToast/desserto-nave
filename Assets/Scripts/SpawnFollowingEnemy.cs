﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFollowingEnemy : MonoBehaviour
{
  public bool isEnabled = true;
  public GameObject enemy;
  public GameObject target;

  private GameObject spawned;

  public int minInterval = 3;
  public int maxInterval = 10;

  // Start is called before the first frame update
  void Start()
  {
    if (isEnabled)
      StartCoroutine(RespawnEnemy());
  }

  IEnumerator RespawnEnemy()
  {
    while (true)
    {
      if (spawned == null)
      {
        var interval = Random.Range(minInterval, maxInterval);
        yield return new WaitForSeconds(interval);

        spawned = Instantiate(enemy, transform.position, Quaternion.identity);
        spawned.GetComponent<MoveEnemy>().target = target.transform;
      }
      yield return null;
    }
  }
}

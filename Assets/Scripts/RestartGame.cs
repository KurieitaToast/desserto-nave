﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine. SceneManagement;
using UnityEngine.InputSystem;
    
public class RestartGame : MonoBehaviour
{
    public GameObject panel;
    private PlayerControls _controls;


    // Start is called before the first frame update
    void Awake()
    {
        Time.timeScale = 0;
        _controls = new PlayerControls();
        _controls.Player0.Any.performed += context => startGame();
        _controls.Player0.Restart.performed += context => restart();
    }
    

    private void OnEnable()
    {
        _controls.Player0.Enable();
    }

    private void OnDisable()
    {
        _controls.Player0.Disable();
    }

    private void startGame()
    {
        Time.timeScale = 1;
        
        panel.SetActive( false);
    }

    private void restart()
    {
        SceneManager.LoadScene(0);
    }
}

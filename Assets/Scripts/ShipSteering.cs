﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class ShipSteering : MonoBehaviour
{
    public float shipMoveSpeed = 2f;

    private Camera _cam;
    private Rigidbody _rb;

    public Transform ship;

    public float shipRotationSpeed = 5f;
    public float accSpeed = 1f;
    public float maxAcc = 3f;
    public float drag = 0.25f;
    private Vector2 _move = Vector2.zero;
    private Vector3 _moveDirection = new Vector3();
    private Vector3 _move3D = new Vector3();
    private float acceleration;
    
    public void SetShip(Transform newShip)
    {
        ship = newShip;
        _rb = ship.GetComponent<Rigidbody>();
        
    }

    private void Awake()
    {
        _cam = Camera.main;
        _moveDirection = _cam.transform.forward;
        _moveDirection.y = 0f;
        _moveDirection.Normalize();
        _move3D = new Vector3(_move.x,0f,_move.y);
    }

    private void OnMove ( InputValue value ) {
        _move = value.Get<Vector2>();      
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void FixedUpdate()
    {
        //_rb.MovePosition(ship.transform.position + (ship.transform.forward * Time.fixedDeltaTime * shipMoveSpeed));

        acceleration += _move.x * Time.deltaTime * accSpeed;
        Mathf.Clamp(acceleration, -maxAcc, maxAcc);

        if (acceleration > 0)
        {
            acceleration -= drag * Time.deltaTime;
        }
        else
        {
            acceleration += drag * Time.deltaTime;
        }
        

        
        Quaternion deltaRotation = Quaternion.AngleAxis(acceleration*shipRotationSpeed*Time.fixedDeltaTime,Vector3.up);
        _rb.MoveRotation(ship.transform.rotation * deltaRotation);
        
    }

    // Update is called once per frame
    void Update()
    {

    }

}

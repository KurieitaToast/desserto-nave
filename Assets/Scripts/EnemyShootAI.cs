﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShootAI : MonoBehaviour
{
  public GameObject bullet;
  public float bulletForce = 20f;
  public float minShotInterval = 1f;
  public float maxShotInterval = 2.5f;

  private Transform pirateShip;
  public Transform shotPositionLeft;
  public Transform shotPositionRight;

  void Start()
  {
    pirateShip = GameObject.FindGameObjectWithTag("PiratShip").transform;
    StartCoroutine(Shoot());
  }

  IEnumerator Shoot()
  {
    while (true)
    {
      bool hit;
      bool isLeftSide;
      RaycastHit hitInfo;
      if (Vector3.Dot(pirateShip.right, pirateShip.position - transform.position) > 0)
      {
        // wir sind links aka Larboard
        //Debug.DrawRay(transform.position, transform.right * 10, Color.red, 0);
        hit = Physics.Raycast(transform.position, transform.right, out hitInfo);
        isLeftSide = true;
      }
      else
      {
        // wir sind rechts aka Starboard
        //Debug.DrawRay(transform.position, -transform.right * 10, Color.red, 0);
        hit = Physics.Raycast(transform.position, -transform.right, out hitInfo);
        isLeftSide = false;
      }
      if (hit && hitInfo.collider != null && hitInfo.collider.gameObject.CompareTag("PiratShip"))
      {
        var shotPosition = isLeftSide ? shotPositionLeft.position : shotPositionRight.position;
        var shot = Instantiate(bullet, shotPosition, Quaternion.identity);
        shot.GetComponent<Rigidbody>().AddForce
        (
          isLeftSide ? transform.right * bulletForce : -transform.right * bulletForce,
          ForceMode.VelocityChange
        );
      }
      var interval = Random.Range(minShotInterval, maxShotInterval);
      yield return new WaitForSeconds(interval);
    }
  }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectScrap : MonoBehaviour
{
    [SerializeField] private int maxScrap;
    [SerializeField] private float scrapOffset = 1f;

    public List<GameObject> scraps;

    private void Awake()
    {
        scraps = new List<GameObject>();
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Scrap") && scraps.Count < maxScrap)
        {
            other.transform.parent = transform;
            
            other.gameObject.GetComponent<Collider>().enabled = false;
            other.gameObject.GetComponent<Rigidbody>().isKinematic = true;
            scraps.Add(other.gameObject);
            other.transform.position = transform.position + transform.up * 2f + transform.up * scrapOffset * scraps.Count;
            other.transform.localScale *= 0.5f;
        }
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleCreator : MonoBehaviour
{

    public GameObject[] obstacles;

    [Range(0,100)]
    public float minRange = 2f;
    [Range( 0, 1000 )]
    public float maxRange = 10f;

    [Range( 0, 100 )]
    public int density = 5;

    private List<Transform> _activeObstacles;


    Transform[] _obstaceList;

    private void Start () {
        _obstaceList = new Transform[density];
        for ( int i = 0; i< _obstaceList.Length; i++ ) {
            _obstaceList[i] = CreateObstacle();
        }
    }

    void Update()
    {
        
        for(int i = 0; i< _obstaceList.Length; i++ ) {
            //maybe delete distant obstacles
            if ( (_obstaceList[i].position - transform.position).sqrMagnitude > maxRange*maxRange ) {
                Destroy( _obstaceList[i].gameObject );
                //create a new one immediately
                _obstaceList[i] = CreateObstacle();
            }
        }

    }

    private Transform CreateObstacle () {
        int idx = Random.Range( 0, obstacles.Length );
        Vector2 offset = Random.insideUnitCircle * (maxRange-minRange);
        offset += offset.normalized * minRange;
        return Instantiate(
            obstacles[idx],
            transform.position + new Vector3(offset.x,0f,offset.y),
            Quaternion.identity).transform;
    } 
}

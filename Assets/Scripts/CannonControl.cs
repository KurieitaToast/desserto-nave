﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.InputSystem;

public class CannonControl : MonoBehaviour
{
    public Transform cannon;
    public GameObject ammo;
  public GameObject canonBlast;
  public Transform shootingPoint;
  public float bulletForce;


    private Vector2 _aim = Vector2.zero;
    private Camera _cam;
    private bool first;

    // Start is called before the first frame update
    void Start()
    {
        _cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if ( _aim.sqrMagnitude>0.2f ) {
            Vector3 pcam = _cam.transform.forward;
            pcam.y = 0f;
            cannon.LookAt( cannon.position+(_cam.transform.right * _aim.x  + pcam.normalized * _aim.y) );
        }*/
        cannon.Rotate(0f, _aim.x * Time.deltaTime * 40f, 0f);

        //_rb.MovePosition( transform.position + (_cam.transform.right * _move.x  + pcam.normalized * _move.y) * Time.fixedDeltaTime * moveSpeed );
    }

    private void OnMove(InputValue value)
    {
        _aim = value.Get<Vector2>();
    }

    private void OnEnter(InputValue value)
    {
        if (!first)
        {
            first = true;
            return;
        }
        CollectScrap cs = GetComponent<CollectScrap>();
        if (cs.scraps.Count <= 0)
        {
            GetComponent<PlayerAudio>().PlayVoiceLine(VoiceLineType.NO_MAT);
            return;
        }

        Destroy(cs.scraps[cs.scraps.Count-1]);
        cs.scraps.RemoveAt(cs.scraps.Count-1);
    //GameObject bullet = Instantiate(
    //    ammo,
    //    cannon.position + cannon.forward * 2f + cannon.up * 1f,
    //    Quaternion.LookRotation(cannon.forward, cannon.up)
    //);
    GameObject bullet = Instantiate(ammo, shootingPoint.position, Quaternion.LookRotation(cannon.forward, cannon.up));
    GameObject blast = Instantiate(canonBlast, shootingPoint.position, Quaternion.LookRotation(cannon.forward, cannon.up));
    
    bullet.GetComponent<Rigidbody>().AddForce(cannon.forward * bulletForce, ForceMode.VelocityChange);
        GetComponent<PlayerAudio>().PlayVoiceLine(VoiceLineType.SHOOT);

    blast.GetComponent<ParticlePlayer>().Play();
  }
  

  private void OnCancel(InputValue value)
    {
    }
}

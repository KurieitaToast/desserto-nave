﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlePlayer : MonoBehaviour
{
  public List<ParticleSystem> particleSystems;

  public Vector3 scale = new Vector3(1f, 1f, 1f);

  public void Play()
  {
    foreach (var ps in particleSystems)
    {
      ps.transform.localScale = scale;
      ps.Play();
    }
  }
  public void Stop()
  {
     foreach (var ps in particleSystems)
     {
       ps.Stop();
     }
  }



}

﻿using Assets.Scripts.DamageSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlane : MonoBehaviour
{
  private SpawnPlayer spawn;

  private void Start()
  {
    var respawn = GameObject.FindGameObjectWithTag("Respawn");
    if (respawn != null)
      spawn = respawn.GetComponent<SpawnPlayer>();
  }

  private void OnCollisionEnter(Collision collision)
  {
    if (collision.gameObject.CompareTag("Player"))
    {
      if (spawn != null)
        collision.gameObject.transform.position = SpawnPlayer.RandomPointInBounds(spawn.spawnArea.bounds);
    }
    else
      Destroy(collision.collider.gameObject);
  }
}

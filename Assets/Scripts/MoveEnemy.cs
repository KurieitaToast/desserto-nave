﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.DamageSystem;


public class MoveEnemy : MonoBehaviour
{
  [SerializeField]
  private int speed = 10;

  public Transform target;

  void Awake()
  {
    if (target != null)
      transform.LookAt(target);
  }

  // Start is called before the first frame update
  void Start()
  {
  }

  // Update is called once per frame
  void Update()
  {
    if (target != null)
    {
      float step = speed * Time.deltaTime;

      //Move
      this.transform.position = Vector3.MoveTowards(transform.position, target.position, step);

      //LookAt
      Vector3 targetDirection = target.position - transform.position;
      Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, step, 0.0f);
      transform.rotation = Quaternion.LookRotation(newDirection);
    }
  }

   
}

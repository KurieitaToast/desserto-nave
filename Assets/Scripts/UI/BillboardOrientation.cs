﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillboardOrientation : MonoBehaviour
{
    private Camera cam;
    private Quaternion startRotation;

    private void Start()
    {
        cam = Camera.main;
        startRotation = transform.rotation;
    }

    private void LateUpdate()
    {
        transform.rotation = cam.transform.rotation * startRotation;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.DamageSystem;

public class MakeDamage : MonoBehaviour
{
    public int hitCount;

    public int damage;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "PiratShip")
        {
            var interactionPointHealths = collision.gameObject.GetComponentsInChildren<InteractionPointHealth>();
            while (hitCount > 0)
            {
                int rnd = Random.Range(0, interactionPointHealths.Length);
                interactionPointHealths[rnd].TakeDamage(1, DamageType.Absolute);
                hitCount--;
            }
        }
        else if (collision.gameObject.CompareTag("Enemy"))
        {
            IDamageable[] targets = collision.gameObject.GetComponentsInChildren<IDamageable>();
            foreach (var tar in targets)
            {
                tar.TakeDamage(damage, DamageType.Absolute);
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed = 100;
    private Camera _cam;
    private Rigidbody _rb;
    private Animator _anim;

    private Vector2 _move = Vector2.zero;

    void Awake()
    {
        _cam = Camera.main;
        _rb = GetComponent<Rigidbody>();
        _anim = GetComponentInChildren<Animator>();
    }

    private void FixedUpdate () {
        //GetComponent<Rigidbody>().WakeUp();
        Vector3 pcam = _cam.transform.forward;
        pcam.y = 0f;
        pcam = _cam.transform.right * _move.x  + pcam.normalized * _move.y;
        _rb.MovePosition( transform.position +  pcam * Time.fixedDeltaTime * moveSpeed );
        if ( pcam.sqrMagnitude>0.1f ) {
            _anim.SetTrigger( "walk" );
            _rb.MoveRotation( Quaternion.LookRotation( pcam, Vector3.up ) );
        } else {
            _anim.SetTrigger( "idle" );
        }     
    }

    private void OnMove ( InputValue value ) {
        _move = value.Get<Vector2>();      
    }

    private void OnEnter ( InputValue value ) {
        Debug.Log( "E" );
    }

    private void OnCancel ( InputValue value ) {
        Debug.Log( "C" );
    }
}

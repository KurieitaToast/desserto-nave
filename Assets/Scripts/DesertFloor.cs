﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesertFloor : MonoBehaviour
{
    public Transform target;

    public GameObject tile;

    private int _lastX = 9999;
    private int _lastY = 9999;

    public int size = 9;

    private Transform[] _tiles;

    public float tileSize = 20f;


    // Start is called before the first frame update
    void Start()
    {
        _tiles = new Transform[(size*size)];
        for( int i=0; i<_tiles.Length; i++ ) {
            _tiles[i] = Instantiate( tile, transform ).transform;
            _tiles[i].localPosition = new Vector3( (i/size)*tileSize, 0, (i%size)*tileSize );
        }
        Debug.Log( "Created "+_tiles.Length );
    }

    // Update is called once per frame
    void Update()
    {
        //checked which tile we're in
        int ts = Mathf.RoundToInt( tileSize );
        int tx = Mathf.RoundToInt( target.position.x ) / ts;
        int ty = Mathf.RoundToInt( target.position.z ) / ts;

        if(tx != _lastX || ty != _lastY ) {
            _lastX = tx;
            _lastY = ty;
            transform.position = new Vector3( (tx-size/2) * tileSize, -20, (ty-size/2) * tileSize);
        }

        

    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Interact : MonoBehaviour
{
    [SerializeField] private float radius;
    public event Action<IInteractable, IInteractable> changedNearstInteractable;
    public event Action<IInteractable> enterdInteractable;
    public event Action<IInteractable> usedInteractable;
    public event Action<IInteractable> leavedInteractable;
    
    private IInteractable currentInteractable;
    private bool enterd = false;
    private void OnEnter(InputValue value)
    {
        if (currentInteractable != null && !enterd)
        {
            currentInteractable.Enter(gameObject);
            enterd = true;
            enterdInteractable?.Invoke(currentInteractable);
        }
        else if(currentInteractable != null && enterd)
        {
            currentInteractable.Act(gameObject);
            usedInteractable?.Invoke(currentInteractable);
        }

    }

    private void Update()
    {
        IInteractable nearst = NearstInteractable();
        if (currentInteractable != null && nearst == null)
        {
            currentInteractable.Leave(gameObject);
            enterd = false;
            leavedInteractable?.Invoke(currentInteractable);
        }
        
        if (nearst != currentInteractable)
        {
            changedNearstInteractable?.Invoke(currentInteractable,nearst);
            currentInteractable = nearst;
        }
        
    }

    private IInteractable NearstInteractable()
    {
        Collider[] cols = Physics.OverlapSphere(transform.position, radius, LayerMask.GetMask("Interactable"));

        GameObject min = null;
        foreach (Collider col in cols)
        {
            if (min == null)
            {
                min = col.gameObject;
            }

            if (Vector3.SqrMagnitude(transform.position - col.transform.position) <
                Vector3.SqrMagnitude(transform.position - min.transform.position))
            {
                min = col.gameObject;
            }
        }

        if (min != null)
        {
            IInteractable[] acts = min.GetComponents<IInteractable>();

            foreach (IInteractable interactable in acts)
            {
                if (interactable.Active) return interactable;
            }
        }

        return null;

    }

    private void OnCancel(InputValue value)
    {
        if (currentInteractable != null)
        {
            currentInteractable.Leave(gameObject);
            enterd = false;
            leavedInteractable?.Invoke(currentInteractable);
        }
    }
}

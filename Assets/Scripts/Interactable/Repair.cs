﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.DamageSystem;
using UnityEngine;
using UnityEngine.UI;

public class Repair : MonoBehaviour, IInteractable
{
    [SerializeField] private bool repairOnStart;
    [SerializeField] private int healthToRepairWithOneScrap = 1;
    private IInteractable[] interactables;
    private bool active = false;
    private InteractionPointHealth health;
    

    public void Start()
    {
        interactables = GetComponents<IInteractable>();
        health = GetComponent<InteractionPointHealth>();
        Active = repairOnStart;
    }

    public void Act(GameObject actor)
    {
        CollectScrap cs = actor.GetComponent<CollectScrap>();
        if (cs.scraps.Count > 0)
        {
            health.Repair(healthToRepairWithOneScrap);
            Destroy(cs.scraps[cs.scraps.Count-1]);
            cs.scraps.RemoveAt(cs.scraps.Count-1);
            actor.GetComponent<PlayerAudio>().PlayVoiceLine(VoiceLineType.REPAIR);

            
            if (health.Health == health.MaxHealth)
            {
                Active = false;
                Leave(actor);
            }
        }
        else
        {
            actor.GetComponent<PlayerAudio>().PlayVoiceLine(VoiceLineType.NO_MAT);
            Leave(actor);
        }
        
    }

    public void Enter(GameObject actor)
    {
        actor.GetComponent<PlayerMovement>().enabled = false;
        Act(actor);
    }

    public void Leave(GameObject actor)
    {
        actor.GetComponent<PlayerMovement>().enabled = true;
    }

    private void ActivateInteractables(bool active)
    {
        foreach (IInteractable interactable in interactables)
        {
            if (interactable.GetType() != typeof(Repair))
            {
                interactable.Active = active;
            }
        }
    }

    public bool Active {
        get => active;
        set
        {
            active = value;
            ActivateInteractables(!value);
        }
        
    }
}
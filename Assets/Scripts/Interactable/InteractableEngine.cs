﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableEngine : MonoBehaviour, IInteractable
{
    public SpringJoint joint;
    private bool active = true;

    public float startSpringValue = 1000;
    public float brokenValue = 250;
    private void Start()
    {
        Active = true;
    }

    public void Act(GameObject actor)
    {
        
    }

    public void Enter(GameObject actor)
    {
        
    }

    public void Leave(GameObject actor)
    {
        
    }

    public bool Active
    {
        get { return active; }
        set
        {
            active = value;
            if (!value)
            {
                joint.spring = brokenValue;
            }
            else
            {
                joint.spring = startSpringValue;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SteeringWheel : MonoBehaviour, IInteractable
{

    private ShipSteering _currentMovement;

    public Transform ship;
    // Start is called before the first frame update
    void Start()
    {
        Active = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Act(GameObject actor)
    {
        
    }

    public void Enter(GameObject actor)
    {
        actor.GetComponent<PlayerMovement>().enabled = false;
        Debug.Log("h");
        _currentMovement=actor.AddComponent<ShipSteering>();
        _currentMovement.SetShip(this.ship);
        
        actor.GetComponent<PlayerAudio>().PlayVoiceLine(VoiceLineType.TAKE_CONTROL);

        //Act(actor);
    }

    public void Leave(GameObject actor)
    {
        Destroy(_currentMovement);
        actor.GetComponent<PlayerMovement>().enabled = true;
        actor.GetComponent<PlayerAudio>().PlayVoiceLine(VoiceLineType.LEAVE_CONTROL);
    }
    


    public bool Active
    {
        get;
        set;
    }
}


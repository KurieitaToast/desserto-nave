﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableDummy : MonoBehaviour, IInteractable
{
    private bool enterd;
    
    private void Awake()
    {
        Active = true;
    }

    public void Act(GameObject actor)
    {
        if(!enterd) return;
        Debug.Log(actor + "interacted with " + gameObject);
    }

    public void Enter(GameObject actor)
    {
        enterd = true;
        actor.GetComponent<PlayerMovement>().enabled = false;
        Debug.Log(actor + " enterd " + gameObject);
    }

    public void Leave(GameObject actor)
    {
        enterd = false;
        actor.GetComponent<PlayerMovement>().enabled = true;
        Debug.Log(actor + " leaved " + gameObject);
    }

    public bool Active { get; set; }
}

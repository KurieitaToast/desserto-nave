﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable
{
    void Act(GameObject actor);

    void Enter(GameObject actor);
    void Leave(GameObject actor);
    bool Active { get; set; }
}

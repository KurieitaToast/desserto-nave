﻿using UnityEngine;

public class InteractableCannon : MonoBehaviour, IInteractable {

    public GameObject cannonBall;
  public GameObject canonBlast;
  public Transform shootingPoint;
  public float bulletForce;

    private bool enterd;

    private void Awake () {
        Active = true;
    }

    public void Act ( GameObject actor ) {
        if ( !enterd ) return;
    }

    public void Enter ( GameObject actor ) {
        enterd = true;
        actor.GetComponent<PlayerMovement>().enabled = false;
        CannonControl cctrl = actor.AddComponent<CannonControl>();
        cctrl.cannon = transform;
        cctrl.ammo = cannonBall;
    cctrl.shootingPoint = shootingPoint;
    cctrl.canonBlast = canonBlast;
    cctrl.bulletForce = bulletForce;


        Debug.Log( actor + " is using " + gameObject );
    }

    public void Leave ( GameObject actor ) {
        enterd = false;
        actor.GetComponent<PlayerMovement>().enabled = true;
        Destroy( actor.GetComponent<CannonControl>() );
        Debug.Log( actor + " stops using " + gameObject );
    }

    public bool Active { get; set; }
}
